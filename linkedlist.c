#include <stdio.h>
#include <stdlib.h>

int add_node(char c);
void print_list(void);
// void liberate_list(void);

struct node
{
	char data;
	struct node *next;
};

struct node *root;
struct node *last;
int size;

int main(int argc, char const *argv[])
{
	char c;

	size = 0;
	last = 0;
	root = malloc(sizeof(struct node));
	free(root);

	root->next = 0;
	root->data = 0;

	while((c = getc(stdin)) != EOF){
		add_node(c);
		print_list();
	}

	// liberate_list();

	return 0;
}

int add_node(char c){
	// fprintf(stdout, "add_node(%c)\n", c);
	if(last == 0){
		root->data = c;
		last = root;
	}
	else{
		last->next = malloc(sizeof(struct node));
		last = last->next;
		free(last);

		if(last == 0){
			return 1;
		}
		last->next = 0;
		last->data = c;
	}

	size++;
	return 0;
}

void print_list(void){
	struct node *current;
	current = root;

	if(current != 0){
		fprintf(stdout, "root");
		while(current != 0){
			fprintf(stdout, " -> %c", current->data);
			current = current->next;
		}
		fprintf(stdout, " <---last\n");
	}
}

// void liberate_list(void){
// 	struct node *temp;
// 	struct node *current;

// 	current = root;
// 	while(current != 0){
// 		temp = current;
// 		current = current->next;
// 		free(temp);
// 	}	
// }
