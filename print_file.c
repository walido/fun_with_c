// Program to work with reading in a file specified on command line

#include <stdio.h>

int main(int argc, char const *argv[])
{
	if(argc != 2){
		printf("usage: %s filename\n", argv[0]);
	}
	else{
		FILE *file = fopen(argv[1], "r");

		if(file == 0){
			printf("Could not open file %s\n", argv[1]);
		}
		else{
			int x;
			while((x = fgetc(file)) != EOF){
				printf("%c", x);
			}
			fclose(file);
		}
	}
	return 0;
}